package dal

import "gorm.io/gorm"

type GormStore struct {
	db *gorm.DB
}

func (s GormStore) Migrate() error {
	return s.db.AutoMigrate(
		new(MetaKeyEntity),
		new(MetaKeyValueEntity),
		new(EventEntity),
		new(EventOccurrenceEntity),
	)
}

func (s GormStore) Event(fingerprint string, labels map[string]string) (*EventEntity, error) {
	ev := new(EventEntity)
	if err := s.db.First(ev, EventEntity{Fingerprint: fingerprint}).Error; err != nil {
		return nil, err
	}
}
