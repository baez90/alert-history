package dal

import (
	"time"

	"gorm.io/gorm"
)

type MetaKeyEntity struct {
	gorm.Model
	Name string `gorm:"uniqueIndex"`
}

type MetaKeyValueEntity struct {
	gorm.Model
	Key   MetaKeyEntity
	Value string
}

type EventEntity struct {
	gorm.Model
	Labels      []MetaKeyValueEntity
	Fingerprint string                  `gorm:"uniqueIndex"`
	Occurrences []EventOccurrenceEntity `gorm:"foreignKey:Fingerprint;references:Fingerprint"`
}

type EventOccurrenceEntity struct {
	gorm.Model
	Annotations []MetaKeyValueEntity
	Status      string
	StartsAt    time.Time
	EndsAt      time.Time
	Fingerprint string
}
