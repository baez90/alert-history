package api_test

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/prometheus/alertmanager/notify"
	promconf "github.com/prometheus/common/config"
	"github.com/prometheus/common/model"

	"gitlab.com/baez90/alert-history/api"
	"gitlab.com/baez90/alert-history/logging"

	"github.com/maxatome/go-testdeep/td"
	"github.com/prometheus/alertmanager/config"
	"github.com/prometheus/alertmanager/notify/webhook"
	"github.com/prometheus/alertmanager/template"
	"github.com/prometheus/alertmanager/types"
)

var (
	testRoutingCfg = api.RoutingCfg{
		NotificationEndpoint: "/notifications",
		MetricsEndpoint:      api.DefaultMetricsEndpoint,
		PprofBaseEndpoint:    api.DefaultPprofBase,
	}
	defaultTmpl *template.Template
)

func init() {
	if tmpl, err := template.FromGlobs(); err != nil {
		panic(err)
	} else {
		if tmpl.ExternalURL, err = url.Parse("http://localhost:8080"); err != nil {
			panic(err)
		}
		defaultTmpl = tmpl
	}
}

func TestWebHookReceiver_HandleAlertNotification(t *testing.T) {
	type fields struct {
		receiver *api.WebHookReceiver
	}
	type args struct {
		alerts   []*types.Alert
		receiver string
	}
	tests := []struct {
		name   string
		args   args
		fields fields
	}{
		{
			name: "Notify about firing alert",
			args: args{
				receiver: "recv1",
				alerts: []*types.Alert{
					{
						Alert: model.Alert{
							Labels: map[model.LabelName]model.LabelValue{
								model.AlertNameLabel: "Canary restarted",
								"team":               "pt-plat",
								"severity":           "warning",
							},
							Annotations: map[model.LabelName]model.LabelValue{
								model.InstanceLabel: "127.0.0.1",
							},
							StartsAt:     time.Now().Add(-12 * time.Minute),
							GeneratorURL: "",
						},
						UpdatedAt: time.Now().Add(-10 * time.Minute),
						Timeout:   false,
					},
				},
			},
			fields: fields{
				receiver: new(api.WebHookReceiver),
			},
		},
		{
			name: "Notify about recovered alert",
			args: args{
				receiver: "recv2",
				alerts: []*types.Alert{
					{
						Alert: model.Alert{
							Labels: map[model.LabelName]model.LabelValue{
								model.AlertNameLabel: "Canary restarted",
								"team":               "pt-plat",
								"severity":           "warning",
							},
							Annotations: map[model.LabelName]model.LabelValue{
								model.InstanceLabel: "127.0.0.1",
							},
							StartsAt:     time.Now().Add(-12 * time.Minute),
							EndsAt:       time.Now().Add(-5 * time.Minute),
							GeneratorURL: "",
						},
						UpdatedAt: time.Now().Add(-10 * time.Minute),
						Timeout:   false,
					},
				},
			},
			fields: fields{
				receiver: new(api.WebHookReceiver),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			addr, listener := setupListener(t)
			mux := http.NewServeMux()
			api.ConfigureMux(testRoutingCfg, tt.fields.receiver, mux)
			srv := &http.Server{
				Handler: mux,
			}
			go func() {
				td.CmpNil(t, td.EqDeeplyError(srv.Serve(listener), http.ErrServerClosed))
			}()
			t.Cleanup(func() {
				td.CmpNoError(t, srv.Shutdown(context.Background()))
			})
			u, err := url.Parse(fmt.Sprintf("http://%s%s", addr, testRoutingCfg.NotificationEndpoint))
			td.CmpNoError(t, err)
			notifier, err := webhook.New(&config.WebhookConfig{
				HTTPConfig:     new(promconf.HTTPClientConfig),
				NotifierConfig: config.NotifierConfig{VSendResolved: true},
				URL:            &config.URL{URL: u},
			}, defaultTmpl, logging.NewTestGoKitLogger(t))
			td.CmpNoError(t, err)
			ctx := notify.WithReceiverName(notify.WithGroupKey(context.Background(), t.Name()), tt.args.receiver)
			_, err = notifier.Notify(ctx, tt.args.alerts...)
			td.CmpNoError(t, err)
		})
	}
}

func setupListener(tb testing.TB) (string, net.Listener) {
	tb.Helper()
	listener, err := net.Listen("tcp", "127.0.0.1:0")
	td.CmpNoError(tb, err)
	tb.Cleanup(func() {
		_ = listener.Close()
	})
	if tcpListener, ok := listener.(*net.TCPListener); ok {
		addr := tcpListener.Addr().(*net.TCPAddr)
		return fmt.Sprintf("%s:%d", addr.IP.String(), addr.Port), listener
	}
	tb.Fatalf("Listener %v not a TCP listener", listener)
	return "", nil
}
