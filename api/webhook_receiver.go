package api

import (
	"encoding/json"
	"log"
	"net/http"
	"net/http/pprof"
	"path"

	"github.com/prometheus/alertmanager/notify/webhook"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const (
	DefaultPprofBase       = "/debug/pprof"
	DefaultMetricsEndpoint = "/metrics"
)

type RoutingCfg struct {
	NotificationEndpoint string
	MetricsEndpoint      string
	PprofBaseEndpoint    string
}

func ConfigureMux(cfg RoutingCfg, r *WebHookReceiver, m *http.ServeMux) {
	m.HandleFunc(cfg.NotificationEndpoint, r.HandleAlertNotification)

	if cfg.PprofBaseEndpoint != "" {
		m.HandleFunc(cfg.PprofBaseEndpoint, pprof.Index)

		m.HandleFunc(path.Join(cfg.PprofBaseEndpoint, "cmdline"), pprof.Cmdline)
		m.HandleFunc(path.Join(cfg.PprofBaseEndpoint, "profile"), pprof.Profile)
		m.HandleFunc(path.Join(cfg.PprofBaseEndpoint, "symbol"), pprof.Symbol)
		m.HandleFunc(path.Join(cfg.PprofBaseEndpoint, "trace"), pprof.Trace)
	}

	if cfg.MetricsEndpoint != "" {
		m.Handle(cfg.MetricsEndpoint, promhttp.Handler())
	}
}

type WebHookReceiver struct {
}

func (r WebHookReceiver) HandleAlertNotification(writer http.ResponseWriter, req *http.Request) {
	if req.Body == nil {
		writer.WriteHeader(http.StatusBadRequest)
		return
	}
	defer req.Body.Close()
	decoder := json.NewDecoder(req.Body)
	var n webhook.Message
	if err := decoder.Decode(&n); err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		return
	}
	log.Println(n.GroupKey)
	writer.WriteHeader(http.StatusOK)
}
