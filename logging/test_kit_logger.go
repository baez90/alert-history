package logging

import (
	"testing"

	kitlog "github.com/go-kit/log"
)

func NewTestGoKitLogger(tb testing.TB) kitlog.Logger {
	return testGoKitLogger{TB: tb}
}

type testGoKitLogger struct {
	testing.TB
}

func (t testGoKitLogger) Log(keyvals ...interface{}) error {
	t.TB.Log(keyvals)
	return nil
}
