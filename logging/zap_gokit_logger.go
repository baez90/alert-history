package logging

import (
	"errors"
	"fmt"
	"reflect"

	kitlog "github.com/go-kit/log"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func NewZapGoKitLogger(logger *zap.Logger) kitlog.Logger {
	return zapGoKitLogger{Logger: logger}
}

type zapGoKitLogger struct {
	Level zapcore.Level
	*zap.Logger
}

func (t zapGoKitLogger) Log(keyvals ...interface{}) error {
	if len(keyvals)%2 == 1 {
		return errors.New("key-values count is odd")
	}

	fields := make([]zap.Field, 0, len(keyvals)/2)

	for i := 0; i < len(keyvals); i += 2 {
		var key string

		switch x := keyvals[i].(type) {
		case string:
			key = x
		case fmt.Stringer:
			key = safeString(x)
		default:
			key = fmt.Sprint(x)
		}

		if i+1 < len(keyvals) {
			fields = append(fields, zap.Any(key, keyvals[i+1]))
		}
	}

	t.Logger.Check(t.Level, "").Write(fields...)
	return nil
}

func safeString(str fmt.Stringer) (s string) {
	defer func() {
		if panicVal := recover(); panicVal != nil {
			if v := reflect.ValueOf(str); v.Kind() == reflect.Ptr && v.IsNil() {
				s = "NULL"
			} else {
				panic(panicVal)
			}
		}
	}()
	s = str.String()
	return
}
